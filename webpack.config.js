var webpack = require("webpack");
var path = require('path');
var helpers = require('./helpers');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        'main': ['./src/app/polyfills', './src/app/main']
    },
    output: {
        path: path.resolve(__dirname, 'www'),
        filename: "./build/[name].js"
    },
    resolve: {
        extensions: ['.js', '.ts']
    },
    devtool: 'source-map',
    devServer: {
        historyApiFallback:{
            index: 'www/index.html'
        }
    },
    module: {
        loaders: [
            {
                test: /\.ts/,
                loaders: ['ts-loader', 'angular2-template-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                use: 'raw-loader',
                exclude: [helpers.root('src/index.html')]
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: ['main']
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        })
    ]
};