import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {HttpModule} from "@angular/http";
import { RouterModule, Routes } from '@angular/router';

import {Contact} from "../features/contact/contact.component";
import {Home} from "../features/home/home.component";

let components = [AppComponent, Home, Contact];
let pipes = [];
let services = [];

const appRoutes: Routes = [
    { path: '', component: Home },
    { path: 'contact', component: Contact },
    { path: '*', component: Home }
];

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        ReactiveFormsModule,
        RouterModule.forRoot(appRoutes)
    ],
    entryComponents: [].concat(components),
    declarations: [].concat(components, pipes),
    providers: [].concat(services),
    bootstrap: [AppComponent]
})
export class AppModule {}